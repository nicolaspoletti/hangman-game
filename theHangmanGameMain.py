import random
import time
from theHangmanStages import *
from theHangmanIntro import *


intro()   # This just imports an ASCII art as an intro.
input()

game_on = True
words = '''good rotten cow dog piano guitar bird animal fish tree green oxygen water'''.lower().split()


def character_positioning(str_word, guess_attempt):

	for i, letter in enumerate(str_word):
		if letter == guess_attempt:
			listed_word_spaces[i] = guess_attempt
	
	return listed_word_spaces


def assembling_string(guessed_list):
	# Converts a guessed of chars into a string.
	
	initial_str = ''.join(guessed_list)

	return initial_str


def get_guess(guessed_letter: str):
	# Returns the letter the played entered. This function makes sure the 
	# player entered a single letter and not something else.

	while True:

		guessed_letter = guessed_letter.lower()
		if len(guessed_letter) != 1:
			print('Please enter a single letter.')
			guessed_letter = input()
		elif guessed_letter in already_guessed:
			print('You have already guess that letter')
			guessed_letter = input()
		elif guessed_letter not in 'abcdefghijklmnopqrstuvwxyz':
			print('Please enter a letter.')
		else:
			return guessed_letter


def play_again():
	print("\n\tDo you want to play again? (y/n)")
	answer = input()

	if answer == "y":
		return True
	else:
		return False


while game_on:

	secret_word = random.sample(words, 1)
	secret_word = secret_word[0]
	number_of_letters = len(secret_word)

	missed_letters = ''
	already_guessed = ''
	misses = 0
	won = False

	print('I\'m thinking of a word...')
	time.sleep(1)
	print(f'\nThe word has {number_of_letters} letters...')
	print(number_of_letters * '_ ')  # Helps the user visualize the word

	word_spaces = number_of_letters * '_'
	listed_word_spaces = list(word_spaces)

	while not won:
		print('\nChoose a letter')
		chosen_letter = input()

		guess = get_guess(chosen_letter)

		if guess in secret_word:
			print('\nYour guess is correct!')
			already_guessed += guess
			correct_guesses_list = character_positioning(secret_word, guess)
			print('Secret word: ', assembling_string(correct_guesses_list))

			if secret_word == assembling_string(correct_guesses_list):
				print("\n YOU WON!")
				won = True

				if play_again():
					break
				else:
					game_on = False

		elif guess not in secret_word:
			print('\nWrong letter, try again.')
			missed_letters += (guess + '-')
			already_guessed += guess
			print('\nMissed letters: ', missed_letters, end=' ')
			misses += 1

			if misses == 1:
				hangmanOne()
			elif misses == 2:
				hangmanTwo()

			elif misses == 3:
				hangmanThree()

			elif misses == 4:
				hangmanFour()

			elif misses == 5:
				hangmanFive()

			elif misses == 6:
				hangmanSix()
				print('You lose! - The word was:' + '"' + secret_word + '"')

				if play_again():
					break
				else:
					won = True
					game_on = False

		else:
			print('Wrong input.')


