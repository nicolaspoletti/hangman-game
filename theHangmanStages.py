
def hangmanZero():
		print('''

	 +---+
	     |
	     |
	     |
	     |
	    ===

		''')

def hangmanOne():
	print('''

	 +---+
	 o   |
	     |
	     |
	     |
	    ===

		''')

def hangmanTwo():
	print('''

	 +---+
	 o   |
	 |   |
	     |
	     |
	    ===

		''')

def hangmanThree():
	print('''

	 +---+
	 o   |
	/|   |
	     |
	     |
	    ===

		''')

def hangmanFour():
	print('''

	 +---+
	 o   |
	/|\  |
	     |
	     |
	    ===

		''')

def hangmanFive():
	print('''

	 +---+
	 o   |
	/|\  |
	/    |
	     |
	    ===

		''')

def hangmanSix():
	print('''

	 +---+
	 o   |
	/|\  |
	/ \  |
	     |
	    ===

		''')

